# Virsh client

## Pool management

`pool-create` allows creation of temporary pools
`pool-define` allows creation of permanent pools

### Creata a new dir pool

```bash
virsh pool-define-as --type dir --target <path_to_dir> --name <name>
```

### Activate the pool at start

```bash
virsh pool-autostart --pool <name>
```

## VMs installation



# TODOs

- [ ] document the difference between virtualization spaces
