# function qemu-drive-from-base() {{{
function qemu-drive-from-base() {
    [ "$#" -ne "2" ] && echo "Not enough args: $0 base_disk dest_disk" >&2 && return 1

    local base_disk="$1"
    local dest_disk="$2"

    [ ! -f "$base_disk" ] && echo "File $base_disk does not exist or is not a regular file" >&2 && return 2
    [ ! -r "$base_disk" ] && echo "File $base_disk is not readable" >&2 && return 3

    local base_ext="${base_disk##*.}"
    local dest_ext="${dest_disk##*.}"

    qemu-img create -F ${dest_ext} -f ${base_ext} -b "${base_disk}" "${dest_disk}" && \
        sudo chown :libvirt "${dest_disk}" && \
        sudo chmod 660 "${dest_disk}"
}
# }}}

# function qemu-get-ip() {{{
function qemu-get-ip() {
    [ "$#" -ne "1" ] && echo "Not enough args: $0 machine_name" >&2 && return 1

    local machine_name="$1"

    while read line; do
        local mname=$(echo "$line" | awk '{print$2}')
        local mindex=$(echo "$line" | awk '{print$1}')

        echo "${mname}: $(virsh --connect=qemu:///system domifaddr ${mindex} | \
            grep ipv4 | \
            head -n 1 | \
            awk '{print$4}' | \
            cut -d/ -f1)"

    done < <(virsh --connect=qemu:///system list | grep "${machine_name}")
}
# }}}


# function qemu-ssh() {{{
function qemu-ssh() {
    [ "$#" -ne "1" ] && echo "Not enough args: $0 user@machine_name" >&2 && return 1 

    local user="${1%%@*}"
    local machine_name="${1##*@}"

    local machine_ip=$(qemu-get-ip "${machine_name}")

    [ -z "${machine_ip}" ] && echo "Machine $machine_name does not exist" >&2 && return 2 

    ssh "${user}@${machine_ip}"
}
# }}}

function qemu-cluster() {
    [ "$#" -ne "2" ] && echo "Usage: $0 (start|stop) cluster_name" >&2 && return 1

    case "$1" in
        start)
            local action="start"
            local look_options="--all"
            local net_state_pattern="inactif"
            local machine_pattern="fermé"
            ;;
        stop)
            local action="destroy"
            local look_options=""
            local net_state_pattern="actif"
            local machine_pattern="en cours d.exécution"
            ;;
        *)
            echo "Unknown action: $1. Should be start or stop." >&2 && return 2
            ;;
    esac

    while read network; do
        virsh --connect=qemu:///system net-${action} --network "${network}"
    done < <(virsh --connect=qemu:///system net-list ${look_options} | grep "^\s*${2}-net\s*${net_state_pattern}\s\+.*$" | awk '{print$1}')

    while read domain; do
        virsh --connect=qemu:///system ${action} --domain "${domain}" &
    done < <(virsh --connect=qemu:///system list ${look_options} | grep "^\s*\([0-9]*\|\-\)\s*secu-os-.*\s*${machine_state_pattern}\s*$" | awk '{print$2}')

    wait
}
